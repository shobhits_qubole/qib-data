namespace :seed do
  task :create_regression_account, [:aws_access_key, :aws_secret_key] => :environment do |task, args|
    include AccountHelper

    # These two methods are a hack to make account creation work
    def cookies
      return Hash.new
    end

    def current_qbol_user
    end

    # 1. Create Qubole regression user
    user = User.create!(
      email: "quboleregression@gmail.com",
      password: "CanopyData",
      name: "Qubole Regression",
      :oauth_provider => "google_oauth2",
      :oauth_uid => SecureRandom.hex(10),
      :given_name => "Qubole",
      :family_name => "Regression"
    )
    user.confirm!

    aws_access_key = args.aws_access_key
    aws_secret_key = args.aws_secret_key

    # 2. Create the account.
    cloud_provider = get_cloud_provider()
    account_name = user.email.split("@")[0]

    args = {}
    args[:account] = cloud_provider.get_account_setup_options()
    args[:account][:name] = account_name
    create_account(args, user)

    account = Account.find_by_name(account_name)

    # 3. Mark the user as super user
    SuperUser.create!(
      user_id: user.id,
      granter_id: user.id,
      description: "Giving myself access",
      expired_at: Time.now + 1.years
    )

    user.is_admin = true
    user.is_super = true
    user.save!

    # 4. Set the auth token for the qbol user
    # in rails console, run QpalHelper.generate_token. That will generate a new token, replace it here.
    qbol_user = user.qbol_users.first
    qbol_user.authentication_token = "dQRK7Cq8DvQATsNpxhwoHDYywTgG8Qv4dZZSfT84zYurcwvb91JjCmzE5ADmqzZk"
    qbol_user.save!

    # 5. Based upon cloud provider, configure cloud creds
    if cloud_provider.name == "AWS"
      puts "rake task: in aws"
      account.idle_cluster_timeout_in_secs = 1800
      account.acc_key = aws_access_key
      account.secret = aws_secret_key

      account.compute_access_key = aws_access_key
      account.compute_secret_key = aws_secret_key

      account.defloc = "qubole-qa-defloc/qib/awsmaster"

      account.storage_type = "CUSTOMER_MANAGED"
      account.compute_type = "CUSTOMER_MANAGED"
      account.storage_validated = true
      account.compute_validated = true
      
      account.save

      account.delete_account_cache('has_storage_creds')
      account.delete_account_cache('has_compute_creds')

      AccountLimit.create!(
        account_id: account.id,
        metric_name: 'Max Clusters',
        limit: 10
      )
      account.create_default_hive_tables
      cluster_config = ClusterConfig.where(hadoop_slave_request_type: 'spot', stable_spot_bid_percentage: [nil, ""])
      cluster_config.each do |config|
        config.stable_spot_bid_percentage = '150'
        config.save!
      end

    elsif cloud_provider.name == "AZURE"
      puts "rake task: in azure"
      account.idle_cluster_timeout_in_secs = 3600
      cred = AzureCred.new
      compute_cred = AzureComputeCred.new
      storage_cred = AzureStorageBlobCred.new
      storage_cred.storage_access_key = "1RJSdbbCM10O6zl0OG26B9BUKO8fuZ34Ca2P09b9CyIUDi5aP43G3bNJooBQ3+l/etbFHnMAmfzpUa+CpagHkw=="
      storage_cred.storage_account_name = "qubolekarma"
      storage_cred.storage_account_container = "defloc@qubolekarma.blob.core.windows.net"
      compute_cred.compute_client_id =  "3c83e997-0c84-4744-b770-96dbf1430655"
      compute_cred.compute_client_secret = "fOguq1bZD6yHD6D8XGH0+mdyRB3Rvv/UqVali5NFjRY="
      compute_cred.compute_tenant_id =  "43d6e5ed-23bd-4a7c-8f52-35b9e8187263"
      compute_cred.compute_subscription_id = "53db48cb-d982-4a6e-ba70-e0998d5d1dc6"
      cred.azure_storage_cred = storage_cred
      cred.azure_compute_cred = compute_cred
      cred.save!

      credential = Credential.new
      credential.owner_id = account.id
      credential.owner_type = Account.to_s
      credential.cred_id = cred.id
      credential.cred_type = AzureCred.to_s
      credential.save!

      account.defloc = "defloc@qubolekarma.blob.core.windows.net"
      account.storage_validated = true
      account.compute_validated = true
      account.save

      account.delete_account_cache('has_storage_creds')
      account.delete_account_cache('has_compute_creds')

      default_cluster = account.clusters.by_tag(:default).first
      default_cluster_cloud_config = default_cluster.cluster_config.cloud_config
      # default_cluster_cloud_config.azure_cred = account.credential.cred
      default_cluster_cloud_config.azure_cred = credential.cred
      default_cluster_cloud_config.save!
      account.create_default_hive_tables
    elsif cloud_provider.name == "ORACLE_BMC"
      puts "rake task: in ORACLE_BMC"
      cloud_cred = OracleBmcCred.new
      cloud_cred.compute_tenant_id = "ocid1.tenancy.oc1..aaaaaaaa3wdgvxsfqtb2ycx7mzeo7hjvl4ogfix5p6b64bp4vjtwc2h5c4fa"
      cloud_cred.compute_user_id = "ocid1.user.oc1..aaaaaaaaaqobg3w4gye2yyjs6xknq33hiavlkffcblfvtqkz67x4oz3itwqq"
      cloud_cred.compute_key_finger_print = "d1:6a:3c:b7:bc:c6:fe:7b:cc:73:c5:e1:66:4d:be:10"
      cloud_cred.compute_api_private_rsa_key = "-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAlpD4tpDmGRWrlIL5bBmwuozwhbxmHDl9QayocKbvTUOZkTl9
q0ySPZuwzE35lGV0a8tvl+MIr3ESIYDI1i0D/jdjHZMJ/ba+9IdOS/XVgIZ+vxWt
QQOIceXDczdUkbyzbLQTOUzm6jGqzS1L8AnEXBaNbhxWqpFx1dWRnq5uLZzbtaJG
GKuj32XeleYBiLCzzS+k685hHrc5rdPpKyR0mPAcgaqML4ZxQC2ZguHcK3XqHPBG
XCWrN36PHezlRklfzX5H9rdw5q12J3cI6APUSndn5rRCX8GY2ekRw0NYsjtiZ8qu
cOSZzYqg5aGho3fiZ70w9AUd9ZWtglYMEvDv+wIDAQABAoIBAFKIXQ0WGEsfydG6
iaZr3qgjoaRtRTurd7GMRziZCQu9RmyIqw4/BtqmgR88jtZ9VnfQJ/eGB7jDrkCX
K3/bZRjwm3AWrZ9ZkQrRMGYK92FgWBG+fB3rJdvpKl35sm3Alkm7293I/braRtn5
Pmrxr6Ua56O3/Pm6iD/CtfPZgaOWRMYOD1Fwg/5nFD0iGqTb1lpentQRCiotWqLc
/QJoHj8gQfVZCWoxRB6dA/yn7TE2w+jjiscrjTbA2m9feOo4BBc4mRNA9g5Fq8IY
RUh2Uow1JndhuZmVYJ8M8vrgIw9rIY2Os8TD0oy3lwoEFompl+v0BkvzqsY9mlIr
Yrg4ykECgYEAx0ykW6/h3sJ/4+FYJbZcQ1GKa4zU9+oE7j2G/22SQANafPNUC2tl
lGTer39NWvJd5SduPz5t78p/Ud/MYdulmLmq30l35lQLdC8ncMqA4RsflIOu407h
JjdKXKUd+cSxgYgcH2moMl9UliVVwA+w7lsqc4Qz7HX62IG+b/ZMgCkCgYEAwWcB
PazF2si7FuxgxBWrJ/OV/n1jvuzBFTFdawqyU1FTmU1TdjmTqbEVtx8PJaEqHJ7M
/1CjjFHYjMwm5XXdK2QBINFSD1pPRddUs3IsTY4YNn5bBU9PJN8fww7JzNhJesPq
oxIgrIWZIDQlOZZI3bgOsMfpNAze+4rMiVvP44MCgYEAnQE+1dA4Y1HciHxlAcio
5IufPV0EBY5BdiHYcO8z8OPzbE4RHrHrozzXxW/HU+dtmvI2QvJ+jxJ6vNhO3aS4
zxktiJwLLdN19oCTVKLjZvMELO6UXxQsAPw7w8J8W+Ddacr9RK3qyEwbifE9nYF/
BDejuL8kXJsRE3Eawn9JAwkCgYBpv2KEUIXWfM8fV5wJWze8nomzHuh9KoLgjZ9g
ufaLa/pj5k3uZMkCjdsrx4XGs3qNL58zMupZ/R6EzdOuv2VxJjsfsJRH5L9Xlsev
Xowu07UVhr1KB/VkWcPKKbnZkaGLyLfw9iZ7boktfJWmCTBKWytdKdGvLiJ2M4xm
xpbC9QKBgDdbWGdUe6spA/TMtBehO/QL/sXtUt3ZOIeq5naElltQDbssEHKSSW0a
i1haB6WNHtEIShI1P/KIs0MeZA4ksKwHBULekDsLNGKL+ktNz9910azTe2fUmrKg
zrR7LqKfy+vGZlTG2HFbf4l1dmBwZdwxRwV4e0y2XWi9cRyCzAiQ
-----END RSA PRIVATE KEY-----"
      cloud_cred.storage_tenant_id = "ocid1.tenancy.oc1..aaaaaaaa3wdgvxsfqtb2ycx7mzeo7hjvl4ogfix5p6b64bp4vjtwc2h5c4fa"
      cloud_cred.storage_user_id = "ocid1.user.oc1..aaaaaaaaaqobg3w4gye2yyjs6xknq33hiavlkffcblfvtqkz67x4oz3itwqq"
      cloud_cred.storage_key_finger_print = "d1:6a:3c:b7:bc:c6:fe:7b:cc:73:c5:e1:66:4d:be:10"
      cloud_cred.storage_api_private_rsa_key = "-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAlpD4tpDmGRWrlIL5bBmwuozwhbxmHDl9QayocKbvTUOZkTl9
q0ySPZuwzE35lGV0a8tvl+MIr3ESIYDI1i0D/jdjHZMJ/ba+9IdOS/XVgIZ+vxWt
QQOIceXDczdUkbyzbLQTOUzm6jGqzS1L8AnEXBaNbhxWqpFx1dWRnq5uLZzbtaJG
GKuj32XeleYBiLCzzS+k685hHrc5rdPpKyR0mPAcgaqML4ZxQC2ZguHcK3XqHPBG
XCWrN36PHezlRklfzX5H9rdw5q12J3cI6APUSndn5rRCX8GY2ekRw0NYsjtiZ8qu
cOSZzYqg5aGho3fiZ70w9AUd9ZWtglYMEvDv+wIDAQABAoIBAFKIXQ0WGEsfydG6
iaZr3qgjoaRtRTurd7GMRziZCQu9RmyIqw4/BtqmgR88jtZ9VnfQJ/eGB7jDrkCX
K3/bZRjwm3AWrZ9ZkQrRMGYK92FgWBG+fB3rJdvpKl35sm3Alkm7293I/braRtn5
Pmrxr6Ua56O3/Pm6iD/CtfPZgaOWRMYOD1Fwg/5nFD0iGqTb1lpentQRCiotWqLc
/QJoHj8gQfVZCWoxRB6dA/yn7TE2w+jjiscrjTbA2m9feOo4BBc4mRNA9g5Fq8IY
RUh2Uow1JndhuZmVYJ8M8vrgIw9rIY2Os8TD0oy3lwoEFompl+v0BkvzqsY9mlIr
Yrg4ykECgYEAx0ykW6/h3sJ/4+FYJbZcQ1GKa4zU9+oE7j2G/22SQANafPNUC2tl
lGTer39NWvJd5SduPz5t78p/Ud/MYdulmLmq30l35lQLdC8ncMqA4RsflIOu407h
JjdKXKUd+cSxgYgcH2moMl9UliVVwA+w7lsqc4Qz7HX62IG+b/ZMgCkCgYEAwWcB
PazF2si7FuxgxBWrJ/OV/n1jvuzBFTFdawqyU1FTmU1TdjmTqbEVtx8PJaEqHJ7M
/1CjjFHYjMwm5XXdK2QBINFSD1pPRddUs3IsTY4YNn5bBU9PJN8fww7JzNhJesPq
oxIgrIWZIDQlOZZI3bgOsMfpNAze+4rMiVvP44MCgYEAnQE+1dA4Y1HciHxlAcio
5IufPV0EBY5BdiHYcO8z8OPzbE4RHrHrozzXxW/HU+dtmvI2QvJ+jxJ6vNhO3aS4
zxktiJwLLdN19oCTVKLjZvMELO6UXxQsAPw7w8J8W+Ddacr9RK3qyEwbifE9nYF/
BDejuL8kXJsRE3Eawn9JAwkCgYBpv2KEUIXWfM8fV5wJWze8nomzHuh9KoLgjZ9g
ufaLa/pj5k3uZMkCjdsrx4XGs3qNL58zMupZ/R6EzdOuv2VxJjsfsJRH5L9Xlsev
Xowu07UVhr1KB/VkWcPKKbnZkaGLyLfw9iZ7boktfJWmCTBKWytdKdGvLiJ2M4xm
xpbC9QKBgDdbWGdUe6spA/TMtBehO/QL/sXtUt3ZOIeq5naElltQDbssEHKSSW0a
i1haB6WNHtEIShI1P/KIs0MeZA4ksKwHBULekDsLNGKL+ktNz9910azTe2fUmrKg
zrR7LqKfy+vGZlTG2HFbf4l1dmBwZdwxRwV4e0y2XWi9cRyCzAiQ
-----END RSA PRIVATE KEY-----"
      cloud_cred.storage_region = "us-phoenix-1"
      cloud_cred.save!

      credential = Credential.new
      credential.owner = account
      credential.cred = cloud_cred
      credential.save!

      account.defloc = "qubole_sandbox@qubole/acc/qib/oracle"
      account.storage_validated = true
      account.compute_validated = true
      account.save

      account.delete_account_cache('has_storage_creds')
      account.delete_account_cache('has_compute_creds')
      account.create_default_hive_tables
    elsif cloud_provider.name == "ORACLE_OPC"
      puts "rake task: in oracle_opc"
      cloud_cred = OracleOpcCred.new
      cloud_cred.username = "/Compute-a431945/somyak@qubole.com",
      cloud_cred.password = "#Qubole05#"
      cloud_cred.save!

      credential = Credential.new
      credential.owner = account
      credential.cred = cloud_cred
      credential.save!
      account.create_default_hive_tables
    end
    begin
      max_clusters = account.get_account_limit('Max Clusters')
      max_clusters.limit = 25.0
      max_clusters.save!
    rescue Exception => e
      logger.error "Exception in updating Max Clusters limit for account: #{account.id}"
      logger.error "Stacktrace: #{e}"
    end
  end
end
